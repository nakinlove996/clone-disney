// Import the functions you need from the SDKs you need
import { initializeApp, getApps, getApp} from "firebase/app";
import { getFirestore } from 'firebase/firestore'
import { getStorage } from 'firebase/storage'
import { getAuth, GoogleAuthProvider } from 'firebase/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDsovU9EDyHB9NL58ykQKWgEgXXQuy5iVk",
  authDomain: "disneyplus-clone-c6f63.firebaseapp.com",
  projectId: "disneyplus-clone-c6f63",
  storageBucket: "disneyplus-clone-c6f63.appspot.com",
  messagingSenderId: "385032013784",
  appId: "1:385032013784:web:a2e835d8a656da826ed4ad"
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const storage = getStorage();
const googleAuthProvider = new GoogleAuthProvider();
const auth = getAuth();

export { app, storage, auth, googleAuthProvider}
export default getFirestore();